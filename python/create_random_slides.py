#!/usr/bin/python3
#**************************************
#**    author: Antonella Succurro    **
#**email:asuccurro[AT]protonmail.com **
#**                                  **
#**    created:       2019/08/25     **
#**    last modified: 2019/08/25     **
#************************************

import argparse
import random
import string
import operator
import os


def main():

    args = options()
    verbose = args.verbose
    titles = args.titles.split(';')
    opening = '\\documentclass{beamer}\n\\usepackage[english]{babel}\n\\usepackage[utf8]{inputenc}\n\\usepackage{pict2e}\n\\usepackage{graphicx}\n\\usepackage{pdfpages}\n\\begin{document}\n\n'
    ending = '\\end{document}'

    path = args.imgpath
    imgs = os.listdir(path)

    
    #figname = '../images/the-treachery-of-images-this-is-not-a-pipe-19482.jpgLarge.jpg'
    for s in range(eval(args.nslides)):
        figs = random.sample(imgs, len(titles))
        fname = f'{args.filename}_%02d.tex' % (s)
        with open(fname, 'w') as outfile:
            outfile.write(opening)
            outfile.write(titleslide(args))
            for t,f in zip(titles, figs):
                outfile.write(slidecode(t, args.imgpath+f))
            outfile.write(ending)
    return

def titleslide(args):

    ts = "\\begin{frame}[plain]{}\\centering\n\LARGE Let's Roll!\\end{frame}\n\n"
    
    return ts

def slidecode(stitle, figname):
    f = figname.split('.')
    fn = '{%s}.%s' % ('.'.join(f[:-1]), f[-1])
    sc = '\\begin{frame}[plain]{%s}\\centering\n\\includegraphics[width=0.8\\textwidth]{%s}' % (stitle, fn)
    sc = sc+'\\end{frame}\n\n'
    return sc

def options():
    '''in-line arguments read by the parser'''
    parser = argparse.ArgumentParser(description='Parsing options')
    parser.add_argument('-V', '--verbose', help='increase output verbosity', action='store_true')
    parser.add_argument('-f', '--filename', help='Output file name', default='../slides/test')
    parser.add_argument('-s', '--nslides', help='', default='1')
    parser.add_argument('-p', '--imgpath', help='', default='../images/')
    parser.add_argument('-t', '--titles', help='', default='Introduction;Setup;Analysis;Results;Conclusions')
    args = parser.parse_args()
    if args.verbose:
        print('Verbosity ON')
        print(args)
    return args

if __name__=="__main__":
    main()
